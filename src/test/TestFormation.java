package test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import appli.Formation;

public class TestFormation {
	Formation forme;
	@Before
	public void initialiser(){
		Map mat = new HashMap<String,Integer>();
		mat.put("r�seau", 2);
		mat.put("algo", 3);
		mat.put("anglais", 1);
		mat.put("PPP",1);
		mat.put("Eco", 2);
		mat.put("C", 3);
		forme = new Formation("Informatique", mat); 
	}
	
	@Test
	public void testCoefMatiere(){
		assertEquals("la valeur donn�e doit etre ",1,forme.coeffMatiere("PPP"));
	}
	@Test
	public void testAjoutMat(){
		forme.ajouterMatiere("Blou", 1000);
		assertTrue("la mati�re n'est pas dans la table ",forme.getLmatiere().containsKey("Blou"));
	}
	@Test
	public void testAjoutMatDejaDedans(){
		forme.ajouterMatiere("PPP", 1000);
		int i =forme.getLmatiere().get("PPP");
		assertEquals("le coeff ne doit pas avoir changer", 1 ,i);
	}
	@Test
	public void testRemoveMat(){
		forme.supprimerMatiere("PPP");
		assertFalse("la mati�re est dans la table ",forme.getLmatiere().containsKey("PPP"));
	}

}
