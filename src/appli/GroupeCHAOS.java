package appli;

import java.util.List;

public class GroupeCHAOS {

	private Formation f;
	private List<Etudiant> le;

	public GroupeCHAOS(Formation forma,List<Etudiant> l){
		this.f=forma;
		this.le = l;
	}

	public Formation getFormation(){
		return f;
	}

	public List<Etudiant> getLEtud(){
		return le;
	}

	public void ajouterEtud(Etudiant et){
		this.le.add(et);
	}

	public void supprimeEtud(Etudiant et){
		this.le.remove(et);
	}

	public float moyenneMat(String s){
		float res = 0;
		for (int i = 0 ; i < le.size();i++){
			res = le.get(i).moyenneMat(s);
		}
		res = res/le.size();
		return res;
	}

	public float moyenneGen(){
		float res = 0;
		for (int i = 0 ; i < le.size();i++){
			res = le.get(i).moyenneGen();
		}
		res = res/le.size();
		return res;
	}
}
