package appli;

import java.util.*;

public class Etudiant {
	private HashMap<String,ArrayList<Float>> lNotes;
	private Identite ident;
	private Formation forma;

	public Etudiant(Identite id,Formation fo){
		this.ident = id;
		this.forma = fo;
		this.lNotes = new HashMap<String,ArrayList<Float>>();
		for(String mat : this.forma.getLmatiere().keySet()){
			this.lNotes.put(mat, new ArrayList<Float>());
		}
	}
	public void ajouterNote(String mat,float not){
		if (0<=not && not>=20 && lNotes.containsKey(mat)){
			this.lNotes.get(mat).add(not);
		}
	}
	public HashMap<String, ArrayList<Float>> getlNotes() {
		return lNotes;
	}
	
	public Identite getIdent() {
		return ident;
	}
	
	public Formation getForma() {
		return forma;
	}
	
	public void ajouterEnsembleNote(String mat,ArrayList<Float> enote){
		int i =0;
		if (lNotes.containsKey(mat)){
			while (i<enote.size()){
				if(0<enote.get(i) && enote.get(i)>20){
					lNotes.get(mat).add(enote.get(i));
				}
				i++;
			}
		}
	}
	public float moyenneMat(String mat){
		float res = 0;
		if (lNotes.containsKey(mat)){
			int i =0;
			while(i<lNotes.get(mat).size()){
				res += lNotes.get(mat).get(i);
				i++;
			}
			res = res/lNotes.get(mat).size();
		}

		return res;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((forma == null) ? 0 : forma.hashCode());
		result = prime * result + ((ident == null) ? 0 : ident.hashCode());
		result = prime * result + ((lNotes == null) ? 0 : lNotes.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etudiant other = (Etudiant) obj;
		if (forma == null) {
			if (other.forma != null)
				return false;
		} else if (!forma.equals(other.forma))
			return false;
		if (ident == null) {
			if (other.ident != null)
				return false;
		} else if (!ident.equals(other.ident))
			return false;
		if (lNotes == null) {
			if (other.lNotes != null)
				return false;
		} else if (!lNotes.equals(other.lNotes))
			return false;
		return true;
	}
	public float moyenneGen(){
		float res =0;
		float coef =0;
		for (String mat : this.forma.getLmatiere().keySet()){
			res += this.moyenneMat(mat);
			coef += this.forma.coeffMatiere(mat);
		}
		return res/coef;
	}
}
