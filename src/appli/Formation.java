package appli;

import java.util.Map;

public class Formation {

	private String id;
	private Map<String,Integer> Lmatiere;

	public Formation(String s ,Map<String,Integer> m){
		this.id = s;
		this.Lmatiere = m;		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((Lmatiere == null) ? 0 : Lmatiere.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Formation other = (Formation) obj;
		if (Lmatiere == null) {
			if (other.Lmatiere != null)
				return false;
		} else if (!Lmatiere.equals(other.Lmatiere))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getId(){
		return this.id;
	}

	public Map<String, Integer> getLmatiere() {
		return Lmatiere;
	}

	@Override
	public String toString() {
		return "Formation [id=" + id + ", Lmatiere=" + Lmatiere + "]";
	}

	public int coeffMatiere(String i){
		int res ;
		if (this.Lmatiere.containsKey(i)){
			res  = this.Lmatiere.get(i);
		}else{
			res = 0;
		}				
		return res;	
	}

	public void ajouterMatiere(String s , int i){
		if (!this.Lmatiere.containsKey(s)){
			this.Lmatiere.put(s, i);
		}
	}
	
	public void supprimerMatiere(String s){
		this.Lmatiere.remove(s);
	}




}
