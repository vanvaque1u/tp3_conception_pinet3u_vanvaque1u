package appli;

import java.util.ArrayList;
import java.util.*;
import java.util.Map;

public class GroupVanvan {
	private Formation form;
	private ArrayList<Etudiant> lEtud;
	
	public GroupVanvan(Formation f,ArrayList<Etudiant> e){
		this.form = f;
			this.lEtud=e;
	}
	public void ajouterEtu(Etudiant e){
		if (e.getForma().getId().equals(this.form.getId())){
			lEtud.add(e);
		}
	}
	public void supprimerEtu(Etudiant e){
		lEtud.remove(e);
	}
	public float moyenneGrpMat(String mat){
		float res =0;
		for (Etudiant e : this.lEtud){
		res += e.moyenneMat(mat);
		}
		return res/lEtud.size();
	}
	public float moyenneGrpGen(){
		float res =0;
		for (Etudiant e : this.lEtud){
			res += e.moyenneGen();
			}
			return res/lEtud.size();
	}
}
